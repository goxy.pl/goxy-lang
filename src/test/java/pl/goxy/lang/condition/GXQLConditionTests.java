package pl.goxy.lang.condition;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pl.goxy.lang.parser.GXQLParser;
import pl.goxy.lang.runtime.GXQLContext;

public class GXQLConditionTests
{
    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    public void condition1(boolean premium)
    {
        GXQLContext context = new GXQLContext();
        context.addVariable("premium", premium);

        GXQLParser parser = new GXQLParser();
        GXQLCondition condition = parser.parseCondition("premium or !premium");

        Assertions.assertTrue(condition.test(context));
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    public void condition2(boolean premium)
    {
        GXQLContext context = new GXQLContext();
        context.addVariable("premium", premium);

        GXQLParser parser = new GXQLParser();
        GXQLCondition condition = parser.parseCondition("premium and !premium");

        Assertions.assertFalse(condition.test(context));
    }

    @Test
    public void condition3()
    {
        GXQLContext context = new GXQLContext();
        context.addVariable("hostname", "voxele.pl");
        context.addVariable("protocol", 755);
        context.addVariable("premium", true);

        GXQLParser parser = new GXQLParser();
        GXQLCondition condition = parser.parseCondition("hostname == \"goxy.pl\" or !(protocol >= 755 and premium and hostname==\"voxele.pl\")");

        Assertions.assertFalse(condition.test(context));
    }

    @Test
    public void condition4()
    {
        GXQLContext context = new GXQLContext();
        context.addVariable("hostname", "voxele.pl");
        context.addVariable("protocol", 755);
        context.addVariable("premium", true);

        GXQLParser parser = new GXQLParser();
        GXQLCondition condition = parser.parseCondition("hostname == \"goxy.pl\" or !(premium)");

        Assertions.assertFalse(condition.test(context));
    }

    @Test
    public void condition5()
    {
        GXQLContext context = new GXQLContext();
        context.addVariable("protocol", 755);
        context.addVariable("premium", false);

        GXQLParser parser = new GXQLParser();
        GXQLCondition condition = parser.parseCondition("!(protocol < 755) and !(!(!(!(!(!(!premium))) or premium)))");

        Assertions.assertTrue(condition.test(context));
    }
}
