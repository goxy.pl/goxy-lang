package pl.goxy.lang.runtime;

import pl.goxy.lang.type.GXQLBoolean;
import pl.goxy.lang.type.GXQLNumber;
import pl.goxy.lang.type.GXQLString;
import pl.goxy.lang.type.GXQLType;

import java.util.HashMap;
import java.util.Map;

public class GXQLContext
{
    private final Map<String, GXQLRuntimeVariable> variables = new HashMap<>();

    public void addVariable(String name, Object value)
    {
        GXQLType type;
        if (value instanceof Number)
        {
            type = GXQLNumber.TYPE;
        }
        else if (value instanceof String)
        {
            type = GXQLString.TYPE;
        }
        else if (value instanceof Boolean)
        {
            type = GXQLBoolean.TYPE;
        }
        else
        {
            throw new IllegalArgumentException("invalid value type");
        }
        this.variables.put(name, new GXQLRuntimeVariable(type, value));
    }

    public GXQLRuntimeVariable getVariable(String name)
    {
        return this.variables.get(name);
    }
}
