package pl.goxy.lang.runtime;

import pl.goxy.lang.type.GXQLType;

public class GXQLRuntimeVariable
{
    private final GXQLType type;
    private Object value;

    public GXQLRuntimeVariable(GXQLType type, Object value)
    {
        this.type = type;
        this.value = value;
    }

    public GXQLType getType()
    {
        return type;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }
}
