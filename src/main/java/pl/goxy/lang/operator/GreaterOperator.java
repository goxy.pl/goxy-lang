package pl.goxy.lang.operator;

import pl.goxy.lang.expression.ValueExpression;
import pl.goxy.lang.runtime.GXQLContext;
import pl.goxy.lang.type.GXQLNumber;

public class GreaterOperator
        implements Operator
{
    @Override
    public boolean compare(ValueExpression first, ValueExpression second, GXQLContext context)
    {
        if (first.getType(context) != GXQLNumber.TYPE || second.getType(context) != GXQLNumber.TYPE)
        {
            throw new RuntimeException("> operator require GXQLNumber type");
        }
        Number firstNumber = (Number) first.getValue(context);
        Number secondNumber = (Number) second.getValue(context);
        return firstNumber.intValue() > secondNumber.intValue();
    }

    @Override
    public String toString()
    {
        return ">";
    }
}
