package pl.goxy.lang.operator;

import pl.goxy.lang.expression.ValueExpression;
import pl.goxy.lang.runtime.GXQLContext;

public class EqualityOperator
        implements Operator
{
    @Override
    public boolean compare(ValueExpression first, ValueExpression second, GXQLContext context)
    {
        if (first.getType(context) != second.getType(context))
        {
            throw new RuntimeException("== operator require the same type");
        }
        return first.getValue(context).equals(second.getValue(context));
    }

    @Override
    public String toString()
    {
        return "==";
    }
}
