package pl.goxy.lang.operator;

import pl.goxy.lang.expression.Expression;
import pl.goxy.lang.expression.ValueExpression;
import pl.goxy.lang.runtime.GXQLContext;

public interface Operator
        extends Expression
{
    boolean compare(ValueExpression first, ValueExpression second, GXQLContext context);
}
