package pl.goxy.lang.parser;

import pl.goxy.lang.condition.ConditionMergeMode;
import pl.goxy.lang.condition.GXQLCondition;
import pl.goxy.lang.expression.Expression;
import pl.goxy.lang.expression.ValueExpression;
import pl.goxy.lang.operator.Operator;
import pl.goxy.lang.runtime.GXQLContext;

public class GXQLParser
{
    private GXQLCondition parseCondition(GXQLReader reader, boolean negation)
    {
        ConditionMergeMode mode = null;

        GXQLCondition parent = null;
        GXQLCondition current = null;

        Expression expression;
        while ((expression = reader.readExpression()) != null)
        {
            if (expression == Expression.AND)
            {
                mode = negation ? ConditionMergeMode.OR : ConditionMergeMode.AND;
                current = null;
                continue;
            }
            if (expression == Expression.OR)
            {
                mode = negation ? ConditionMergeMode.AND : ConditionMergeMode.OR;
                current = null;
                continue;
            }
            if (expression == Expression.NEGATION)
            {
                negation = !negation;
                continue;
            }
            if (expression == Expression.GROUP_OPEN)
            {
                if ((mode != null && parent == null) || (mode == null && parent != null))
                {
                    throw new IllegalStateException("invalid format");
                }
                if (parent == null)
                {
                    parent = this.parseCondition(reader, negation);
                }
                else
                {
                    parent.addCondition(mode, this.parseCondition(reader, negation));
                }
                negation = false;
                mode = null;
                continue;
            }
            if (expression == Expression.GROUP_CLOSE)
            {
                if (current != null && mode != null)
                {
                    parent.addCondition(mode, current);
                }
                return parent;
            }
            if (!(expression instanceof Operator || expression instanceof ValueExpression))
            {
                throw new IllegalArgumentException("invalid format");
            }
            if ((current == null || (current.getOperator() != null && current.getSecond() == null)) && expression instanceof Operator)
            {
                throw new IllegalArgumentException("invalid format");
            }
            if (current != null && expression instanceof Operator)
            {
                if (current.getOperator() != null)
                {
                    throw new IllegalArgumentException("invalid format");
                }
                current.setOperator((Operator) expression);
            }
            else if (current != null && current.getOperator() != null)
            {
                current.setSecond((ValueExpression) expression);
                current = null;
            }
            else if (current == null || current.getOperator() == null)
            {
                current = new GXQLCondition(negation, (ValueExpression) expression);
                if (parent == null)
                {
                    parent = current;
                }
                else if (mode != null)
                {
                    parent.addCondition(mode, current);
                }
                mode = null;
            }
        }
        return parent;
    }

    public GXQLCondition parseCondition(String input)
    {
        GXQLReader reader = new GXQLReader(input);
        return this.parseCondition(reader, false);
    }
}
