package pl.goxy.lang.parser;

import pl.goxy.lang.GXQL;
import pl.goxy.lang.expression.Expression;
import pl.goxy.lang.expression.StaticValue;
import pl.goxy.lang.expression.Variable;
import pl.goxy.lang.operator.EqualityOperator;
import pl.goxy.lang.operator.GreaterEqualOperator;
import pl.goxy.lang.operator.GreaterOperator;
import pl.goxy.lang.operator.LessEqualOperator;
import pl.goxy.lang.operator.LessOperator;
import pl.goxy.lang.operator.Operator;
import pl.goxy.lang.type.GXQLNumber;
import pl.goxy.lang.type.GXQLString;

public class GXQLReader
{
    private String input;
    private int length;

    private int position;

    public GXQLReader(String input)
    {
        this.input = input;
        this.length = this.input.length();
    }

    public Expression readExpression()
    {
        int startIndex = this.position;
        while (this.position < this.length)
        {
            char c = this.input.charAt(this.position);
            boolean begin = this.position - startIndex == 0;
            if (c == ' ' && begin)
            {
                this.skipSpaces();
                return this.readExpression();
            }
            if ((c == '>' || c == '=' || c == '<') && begin)
            {
                return this.readOperator();
            }
            if (c == '\"' && begin)
            {
                return this.readString();
            }
            if (((c >= '0' && c <= '9') || c == '-') && begin)
            {
                return this.readNumber();
            }
            if (c == '(' && begin)
            {
                this.position++;
                return Expression.GROUP_OPEN;
            }
            if (c == ')' && begin)
            {
                this.position++;
                return Expression.GROUP_CLOSE;
            }
            if (c == '!' && begin)
            {
                this.position++;
                return Expression.NEGATION;
            }
            if (GXQL.isSpecialCharacter(c))
            {
                break;
            }
            this.position++;
        }
        if (this.position - startIndex == 0)
        {
            return null;
        }
        String name = this.input.substring(startIndex, this.position);
        if (name.equalsIgnoreCase("and"))
        {
            return Expression.AND;
        }
        if (name.equalsIgnoreCase("or"))
        {
            return Expression.OR;
        }
        return new Variable(name);
    }

    public Operator readOperator()
    {
        if (this.position < this.length)
        {
            char firstChar = this.input.charAt(this.position);
            char secondChar = this.position + 1 < this.length ? this.input.charAt(this.position + 1) : ' ';

            if (firstChar == '>' && secondChar == '=')
            {
                this.position += 2;
                return new GreaterEqualOperator();
            }
            else if (firstChar == '>')
            {
                this.position++;
                return new GreaterOperator();
            }
            else if (firstChar == '<' && secondChar == '=')
            {
                this.position += 2;
                return new LessEqualOperator();
            }
            else if (firstChar == '<')
            {
                this.position++;
                return new LessOperator();
            }
            else if (firstChar == '=' && secondChar == '=')
            {
                this.position += 2;
                return new EqualityOperator();
            }
        }
        throw new IllegalArgumentException("invalid");
    }

    public StaticValue readString()
    {
        if (this.position + 1 < this.length && this.input.charAt(this.position) == '\"')
        {
            int nextQuotationMark = this.input.indexOf('\"', this.position + 1);
            if (nextQuotationMark >= 0)
            {
                String value = this.input.substring(this.position + 1, nextQuotationMark);
                this.position = nextQuotationMark + 1;
                return new StaticValue(GXQLString.TYPE, value);
            }
        }
        throw new IllegalArgumentException("invalid");
    }

    public StaticValue readNumber()
    {
        int startPosition = this.position;
        while (this.position < this.length)
        {
            char c = this.input.charAt(this.position);
            if ((c >= '0' && c <= '9') || c == '-')
            {
                this.position++;
                continue;
            }
            break;
        }
        if (this.position - startPosition == 0)
        {
            throw new IllegalArgumentException("invalid");
        }
        String textValue = this.input.substring(startPosition, this.position);
        int value = Integer.parseInt(textValue);
        return new StaticValue(GXQLNumber.TYPE, value);
    }

    private void skipSpaces()
    {
        while (this.position < this.length && this.input.charAt(this.position) == ' ')
        {
            this.position++;
        }
    }

    public int getPosition()
    {
        return position;
    }
}
