package pl.goxy.lang;

public interface GXQL
{
    char[] SPECIAL_CHARACTERS = new char[] { ' ', '=', '-', '\"', '+', '!', ',', '.', '(', ')' };

    static boolean isSpecialCharacter(char c)
    {
        for (char specialCharacter : SPECIAL_CHARACTERS)
        {
            if (c == specialCharacter)
            {
                return true;
            }
        }
        return false;
    }
}
