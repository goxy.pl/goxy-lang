package pl.goxy.lang.type;

public class GXQLBoolean
    implements GXQLType
{
    public static final GXQLBoolean TYPE = new GXQLBoolean();

    private GXQLBoolean()
    {
    }

    @Override
    public String toString()
    {
        return "GXQLBoolean";
    }
}
