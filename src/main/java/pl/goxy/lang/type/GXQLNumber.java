package pl.goxy.lang.type;

public class GXQLNumber
    implements GXQLType
{
    public static final GXQLNumber TYPE = new GXQLNumber();

    private GXQLNumber()
    {
    }

    @Override
    public String toString()
    {
        return "GXQLNumber";
    }
}
