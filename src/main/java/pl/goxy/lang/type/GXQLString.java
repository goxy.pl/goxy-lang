package pl.goxy.lang.type;

public class GXQLString
    implements GXQLType
{
    public static final GXQLString TYPE = new GXQLString();

    private GXQLString()
    {
    }

    @Override
    public String toString()
    {
        return "GXQLString";
    }
}
