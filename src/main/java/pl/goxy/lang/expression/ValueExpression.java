package pl.goxy.lang.expression;

import pl.goxy.lang.runtime.GXQLContext;
import pl.goxy.lang.type.GXQLType;

public interface ValueExpression
        extends Expression
{
    GXQLType getType(GXQLContext context);

    Object getValue(GXQLContext context);
}
