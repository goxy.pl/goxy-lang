package pl.goxy.lang.expression;

import pl.goxy.lang.runtime.GXQLContext;
import pl.goxy.lang.type.GXQLType;

public class StaticValue
        implements ValueExpression
{
    private final GXQLType type;
    private final Object value;

    public StaticValue(GXQLType type, Object value)
    {
        this.type = type;
        this.value = value;
    }

    @Override
    public Object getValue(GXQLContext context)
    {
        return value;
    }

    @Override
    public GXQLType getType(GXQLContext context)
    {
        return this.type;
    }

    @Override
    public String toString()
    {
        return "StaticValue{" + "type=" + type + ", value=" + value + '}';
    }
}
