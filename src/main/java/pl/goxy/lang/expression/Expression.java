package pl.goxy.lang.expression;

public interface Expression
{
    Expression AND = new DefaultExpression("AND");
    Expression OR = new DefaultExpression("OR");
    Expression GROUP_OPEN = new DefaultExpression("GROUP_OPEN");
    Expression GROUP_CLOSE = new DefaultExpression("GROUP_CLOSE");
    Expression NEGATION = new DefaultExpression("NEGATION");

    class DefaultExpression implements Expression
    {
        private final String name;

        public DefaultExpression(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return this.name;
        }
    }
}
