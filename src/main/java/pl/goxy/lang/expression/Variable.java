package pl.goxy.lang.expression;

import pl.goxy.lang.runtime.GXQLContext;
import pl.goxy.lang.runtime.GXQLRuntimeVariable;
import pl.goxy.lang.type.GXQLType;

public class Variable
        implements ValueExpression
{
    private final String name;

    public Variable(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    @Override
    public GXQLType getType(GXQLContext context)
    {
        GXQLRuntimeVariable variable = context.getVariable(this.name);
        if (variable == null)
        {
            throw new IllegalArgumentException("variable is not initialized yet");
        }
        return variable.getType();
    }

    @Override
    public Object getValue(GXQLContext context)
    {
        GXQLRuntimeVariable variable = context.getVariable(this.name);
        if (variable == null)
        {
            throw new IllegalArgumentException("variable is not initialized yet");
        }
        return variable.getValue();
    }

    @Override
    public String toString()
    {
        return "Variable{" + "name='" + name + '\'' + '}';
    }
}
