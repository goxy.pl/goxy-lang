package pl.goxy.lang.condition;

public enum ConditionMergeMode
{
    AND,
    OR
}
