package pl.goxy.lang.condition;

import pl.goxy.lang.expression.ValueExpression;
import pl.goxy.lang.operator.Operator;
import pl.goxy.lang.runtime.GXQLContext;
import pl.goxy.lang.type.GXQLBoolean;
import pl.goxy.lang.type.GXQLType;

import java.util.ArrayList;
import java.util.List;

public class GXQLCondition
{
    private final boolean negation;
    private final ValueExpression first;
    private Operator operator;
    private ValueExpression second;

    private List<ConditionWithMode> conditionList;

    public GXQLCondition(boolean negation, ValueExpression first)
    {
        this.negation = negation;
        this.first = first;
    }

    public ValueExpression getFirst()
    {
        return first;
    }

    public Operator getOperator()
    {
        return operator;
    }

    public void setOperator(Operator operator)
    {
        this.operator = operator;
    }

    public ValueExpression getSecond()
    {
        return second;
    }

    public void setSecond(ValueExpression second)
    {
        this.second = second;
    }

    public void addCondition(ConditionMergeMode mode, GXQLCondition condition)
    {
        if (this.conditionList == null)
        {
            this.conditionList = new ArrayList<>();
        }
        this.conditionList.add(new ConditionWithMode(mode, condition));
    }

    public boolean test(GXQLContext context)
    {
        boolean value;
        if (this.operator == null)
        {
            GXQLType type = this.first.getType(context);
            if (type != GXQLBoolean.TYPE)
            {
                throw new IllegalArgumentException("not boolean");
            }
            value = (boolean) this.first.getValue(context);
        }
        else
        {
            value = this.operator.compare(this.first, this.second, context);
        }
        if (this.negation)
        {
            value = !value;
        }
        if (this.conditionList == null)
        {
            return value;
        }
        for (ConditionWithMode conditionWithMode : this.conditionList)
        {
            if (conditionWithMode.mode == ConditionMergeMode.AND)
            {
                if (!value)
                {
                    return value;
                }
                value = conditionWithMode.condition.test(context);
            }
            else if (conditionWithMode.mode == ConditionMergeMode.OR)
            {
                if (value)
                {
                    return value;
                }
                value = conditionWithMode.condition.test(context);
            }
        }
        return value;
    }

    @Override
    public String toString()
    {
        String condition;
        if (this.operator == null && this.second == null)
        {
            condition = this.first.toString();
        }
        else
        {
            condition = this.first + " " + this.operator + " " + this.second;
        }
        return "GXQLCondition{" + "negation=" + negation + ", condition=" + condition + ", conditionList=" + conditionList + '}';
    }

    static class ConditionWithMode
    {
        ConditionMergeMode mode;
        GXQLCondition condition;

        public ConditionWithMode(ConditionMergeMode mode, GXQLCondition condition)
        {
            this.mode = mode;
            this.condition = condition;
        }

        @Override
        public String toString()
        {
            return "ConditionWithMode{" + "mode=" + mode + ", condition=" + condition + '}';
        }
    }
}
